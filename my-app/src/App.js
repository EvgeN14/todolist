import React, { useEffect, useState } from "react";
import "./App.css";
import toDoData from "./ToDoList/ToDoData";
import ToDoList from "./ToDoList/ToDoList";
import Header from "./Header";
import { Container, Row, Col, Button, FormControl } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPen } from "@fortawesome/free-solid-svg-icons";

const App = () => {
  let tasks;

  if (
    localStorage.getItem("todos") &&
    JSON.parse(localStorage.getItem("todos")).length >= 1
  ) {
    tasks = JSON.parse(localStorage.getItem("todos"));
  } else {
    tasks = toDoData;
    localStorage.setItem("todos", JSON.stringify(tasks));
  }

  const [todos, setTodos] = useState(tasks);

  const [value, setValue] = useState("");
  useEffect(() => {
    localStorage.setItem("todos", JSON.stringify(todos));
  });

  const toggleTodos = (id) => {
    const index = todos.map((item) => item.id).indexOf(id);
    todos[index].completed = !todos[index].completed;

    setTodos([...todos]);
  };

  const addTodos = () => {
    if (value) {
      setTodos([
        ...todos,
        {
          id: Date.now(),
          text: value,
          completed: false,
          datatype: value,
        },
      ]);
    } else {
      alert("i need task");
    }
    document.querySelector("input").value = "  ";
  };

  const deleteTodos = (id) => {
    const addTodos = todos.filter((todos) => todos.id !== id);
    setTodos(addTodos);
  };

  const activeTasks = todos.filter((task) => task.completed === false).length;
  const completedTasks = todos.filter((task) => task.completed === true).length;
  const totalTasks = todos.length;

  const [filtred, setFiltred] = useState(todos);

  useEffect(() => {
    setFiltred(todos);
  }, [todos]);

  const filterTodo = (completed) => {
    if (completed === "all") {
      setFiltred(todos);
    } else {
      let newTodo = [...todos].filter((item) => item.completed == !completed);
      setFiltred(newTodo);
    }
  };

  return (
    <Container className="container">
      <Header />
      <Row>
        <Col className="addnew">
          <FormControl
            type="text"
            placeholder="What needs to be done?"
            onChange={(e) => setValue(e.target.value)}
          />
          <Button className="addbtn" variant="warning" onClick={addTodos}>
            <FontAwesomeIcon icon={faPen} />
          </Button>
        </Col>
      </Row>

      <div>
        {filtred.map((item) => (
          <ToDoList
            key={item.id}
            value={value}
            complited={item.completed}
            description={item.text}
            handelChange={() => toggleTodos(item.id)}
            handelDelete={() => deleteTodos(item.id)}
          />
        ))}
      </div>
      <div className="allbtn">
        <Button
          variant="outline-light"
          type="btnAll"
          onClick={() => filterTodo("all")}
        >
          All {totalTasks}
        </Button>
        <Button
          variant="outline-light"
          type="btnActive"
          onClick={() => filterTodo(true)}
        >
          Active {activeTasks}
        </Button>
        <Button
          variant="outline-light"
          type="btnCompleted"
          onClick={() => filterTodo(false)}
        >
          Completed {completedTasks}
        </Button>
      </div>
    </Container>
  );
};

export default App;
