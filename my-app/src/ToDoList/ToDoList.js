import "./styles.css";
import { Button } from "react-bootstrap";

const ToDoList = (props) => {
  const resolvedClass = {
    textDecoration: "line-through",
    color: "pink",
  };

  return (
    <div className="listtodo">
      <input
        type="checkbox"
        className="btncheck"
        onChange={props.handelChange}
        defaultChecked={props.complited}
      />
      <p
        className="stringtext"
        style={props.complited == true ? resolvedClass : {}}
      >
        {props.description}
      </p>

      <Button
        variant="primary"
        size="lg"
        className="btndelet"
        onClick={props.handelDelete}
      >
        Del
      </Button>
    </div>
  );
};

export default ToDoList;
